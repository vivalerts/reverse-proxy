#!/bin/sh -e

# If GeoIP2 databases exist
if ls /geoip2/*.mmdb
then
  sed -i 's/#GEOIP2//' /etc/nginx/nginx.conf /etc/nginx/conf.d/default.conf
fi

# If certificates don't exist
CERTSDIR=/etc/letsencrypt/live/vivalerts.com
if [ ! -d "$CERTSDIR" ]
then
  # If we are in the production host
  if [ "$TARGET_HOST" = "vivalerts.com" ]
  then
    sed -i "s~$CERTSDIR~/temporary-certificate~" /etc/nginx/conf.d/default.conf

    # The following doesn't block Nginx (first) execution
    # because it's needed to be up for certificate creation.
    until curl -fsS localhost
    do
      sleep 1
    done &&
    certbot --nginx --non-interactive --agree-tos --email pothitos@di.uoa.gr \
            --domain vivalerts.com --domain www.vivalerts.com &
  else
    mkdir -p "$CERTSDIR"
    cp /temporary-certificate/*.pem "$CERTSDIR"
  fi
fi

# Schedule certificates renewal
if ! crontab -l | grep "certbot renew"
then
  (crontab -l; echo "@daily certbot renew") | crontab -
fi

# Output cron jobs without blocking
crond -f &
